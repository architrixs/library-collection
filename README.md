# Library Collection

Contains Different  .tgz packed Libraries to be used Using NPM.

## Getting started

### How to use :

- Install the library using npm install
  ```
  npm install <tarball url>
Example
```
npm install "https://gitlab.com/architrixs/library-collection/-/raw/main/v1/architrixs-annotorious-openseadragon-1.0.0.tgz"
```

## current libray in collection:
### V1 (orginal version used, updated by me)
- Annotorious (2.7.6, 1.0.0) `Added suppport for line shape and marker arrows`
- Annotorious-OpenSeadragon Plugin (2.7.6, 1.0.0) `Added suppport for line shape`
- Annotorious-selector-pack (0.5.3, 0.0.1) `Added line shape`
- Annotorious-toolbar (1.1.0, 1.0.0) `Added line shape button`

### V2 
- Annotorious (2.7.13, 1.0.1) `Added suppport for polyline shape; Added support for custom line`
- Annotorious-OpenSeadragon Plugin (2.7.13, 1.0.1) `Added suppport for polyline shape, Added support for custom line`
- Annotorious-selector-pack (0.5.4, 0.0.2) `Added polyline shape, Added customline shape`
